package com.gsb.suividevosfrais;

import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;

import org.json.JSONArray;

public abstract class Global {

	// tableau d'informations mémorisées
	public static Hashtable<Integer, FraisMois> listFraisMois = new Hashtable<Integer, FraisMois>() ;

	// fichier contenant les informations sérialisées
	public static final String filename = new String("save.fic") ;

	// Variable contenant l'id du visiteur connecté
	public static String id = "0";

	/**
	 * Modification de l'affichage de la date (juste le mois et l'année, sans le jour)
	 */
	public static void changeAfficheDate(DatePicker datePicker) {
		try {
		    Field f[] = datePicker.getClass().getDeclaredFields();
		    for (Field field : f) {
		        if (field.getName().equals("mDaySpinner")) {
		            field.setAccessible(true);
		            Object dayPicker = new Object();
		            dayPicker = field.get(datePicker);
		            ((View) dayPicker).setVisibility(View.GONE);
		        }
		    }
		} catch (SecurityException e) {
		    Log.d("ERROR", e.getMessage());
		} catch (IllegalArgumentException e) {
		    Log.d("ERROR", e.getMessage());
		} catch (IllegalAccessException e) {
		    Log.d("ERROR", e.getMessage());
		}	
	}

	/**
	 * Conversion de l'objet au format JSONArray
	 * @return
	 */
	public static JSONArray convertToJSONArray (Integer key) {
		List list = new ArrayList();
		FraisMois fraisMois = listFraisMois.get(key);
		list.add(id);
		list.add(key);
		list.add(fraisMois.getKm());
		list.add(fraisMois.getEtape());
		list.add(fraisMois.getNuitee());
		list.add(fraisMois.getRepas());
		String annee = key.toString().substring(0,4);
		String mois = key.toString().substring(4);
		String date = "/" + mois + "/" + annee;
		// Ajout à la liste des fraisHF
		Integer nombreFraisHF = fraisMois.getLesFraisHf().size();
		for (Integer i = 0 ; i< nombreFraisHF ; i++){
			Integer montant = fraisMois.getLesFraisHf().get(i).getMontant();
			String motif = fraisMois.getLesFraisHf().get(i).getMotif();
			String jour = fraisMois.getLesFraisHf().get(i).getJour().toString();
			//Si le jour a une longueur de 1 alors on rajoute 0 devant pour avoir un format jj/MM/YYYY
			if (jour.length() == 1){
				jour = "0" + jour;
			}
			String dateFrais = jour + date;
			list.add(montant);
			list.add(motif);
			list.add(dateFrais);
		}
		return new JSONArray(list);

	}
	
}
