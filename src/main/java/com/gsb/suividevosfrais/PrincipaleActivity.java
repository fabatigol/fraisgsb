package com.gsb.suividevosfrais;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Hashtable;

import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.support.annotation.RequiresApi;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONArray;
import org.json.JSONException;

import static android.R.id.list;

public class PrincipaleActivity extends Activity {

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // récupération des informations sérialisées
        recupSerialize();
        // chargement des méthodes événementielles
        cmdMenu_clic(((Button) findViewById(R.id.cmdKm)), KmActivity.class);
        cmdMenu_clic(((Button) findViewById(R.id.cmdRepas)), RepasActivity.class);
        cmdMenu_clic(((Button) findViewById(R.id.cmdNuitee)), NuitActivity.class);
        cmdMenu_clic(((Button) findViewById(R.id.cmdEtape)), EtapesActivity.class);
        cmdMenu_clic(((Button) findViewById(R.id.cmdHf)), HfActivity.class);
        cmdMenu_clic(((Button) findViewById(R.id.cmdHfRecap)), HfRecapActivity.class);

        cmdTransfert_clic();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    /**
     * Récupère la sérialisation si elle existe
     */
    private void recupSerialize() {
        Global.listFraisMois = (Hashtable<Integer, FraisMois>) Serializer.deSerialize(Global.filename, PrincipaleActivity.this);
        // si rien n'a été récupéré, il faut créer la liste
        if (Global.listFraisMois == null) {
            Global.listFraisMois = new Hashtable<Integer, FraisMois>();
        }
    }

    /**
     * Sur la sélection d'un bouton dans l'activité principale ouverture de l'activité correspondante
     */
    private void cmdMenu_clic(Button button, final Class classe) {
        button.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                // ouvre l'activité
                Intent intent = new Intent(PrincipaleActivity.this, classe);
                startActivity(intent);
            }
        });
    }

    /**
     * Cas particulier du bouton pour le transfert d'informations vers le serveur
     */
    private void cmdTransfert_clic() {
        ((Button) findViewById(R.id.cmdTransfert)).setOnClickListener(new Button.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            public void onClick(View v) {
                // envoi les informations sérialisées vers le serveur
                DateFormat dateFormat = new SimpleDateFormat("yyyyMM");
                Date laDate = new Date();
                Integer key = Integer.parseInt(dateFormat.format(laDate));
                AccesDistant accesDistant = new AccesDistant();
                accesDistant.envoi("enregistrer", Global.convertToJSONArray(key));
                Toast.makeText(PrincipaleActivity.this, "Enregistrement en Base",Toast.LENGTH_LONG).show();

            }
        });
    }


}
