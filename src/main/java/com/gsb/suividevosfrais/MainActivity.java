package com.gsb.suividevosfrais;


import android.os.Bundle;
import android.app.Activity;
import java.lang.String;
import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;

import static java.lang.Boolean.FALSE;

public class MainActivity extends Activity {
    // informations affichées dans l'activité
    private EditText login ;
    private EditText mdp ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        valoriseProprietes();
        cmdValider_clic();
    }

    /**
     * Valorisation des propriétés avec les informations affichées
     */
    private void valoriseProprietes() {
        login = (EditText)findViewById(R.id.txtLogin);
        mdp = (EditText)findViewById(R.id.txtPassword);
    }


    /**
     * Sur le clic du bouton valider : connexion
     */
    private void cmdValider_clic() {
        ((Button)findViewById(R.id.btnConnexion)).setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                String loginSaisi = login.getText().toString();
                String mdpSaisi = mdp.getText().toString() ;
                if(loginSaisi.equals("") || mdpSaisi.equals("")) {
                    Toast.makeText(MainActivity.this, "Login et mot de passe doivent être renseignés",Toast.LENGTH_LONG).show();
                }else {
                    List list = new ArrayList();
                    list.add(loginSaisi);
                    list.add(mdpSaisi);
                    JSONArray JsonId = new JSONArray(list);
                    AccesDistant accesDistant = new AccesDistant();
                    accesDistant.envoi("connexion", JsonId);
                    connexion();
                }

            }
        }) ;
    }

    /**
     * Vérification du résultat en fonction du retour
     *
     */
     private void connexion(){
         Toast.makeText(MainActivity.this, "Connexion en cours",Toast.LENGTH_SHORT).show();
         if (Global.id == null ){
                Toast.makeText(MainActivity.this, "Login ou mot de passe incorrect",Toast.LENGTH_LONG).show();
         }else if (Global.id != null && Global.id != "0"){
                retourActivityPrincipale();
         }
     }

    /**
     * Accès écran d'acceuil si le login est correct
     */
    private void retourActivityPrincipale() {
        Intent intent = new Intent(MainActivity.this, PrincipaleActivity.class) ;
        startActivity(intent) ;
    }

}
