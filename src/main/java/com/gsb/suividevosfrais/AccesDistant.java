package com.gsb.suividevosfrais;

/**
 * Created by Fabien on 16/12/2016.
 */

import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;


public class AccesDistant  implements  AsyncResponse{

    // propriétés
    private static final String SERVERADDR = "http://btssio.fabienroby.fr/controleurs/c_android.php" ;

    /**
     * Constructeur
     */
    public AccesDistant() {
        super();
    }

    @Override
    public void processFinish(String output) {
        // affichage console du retour du serveur, pour contrôler
        Log.d("serveur", "************" + output);
        // split output (1ere partie = instruction, 2eme partie = valeur à traiter)
        String[] message = output.split("%");
        // appel des bons traitements en fonction du retour du serveur
        if (message.length > 1) {
            // le serveur a eu un problème d'accès à la base de données
            if (message[0].equals("Erreur !")) {
                Log.d("serveur", "************Erreur :" + message[1]);
                // le serveur a retourné l'idVisiteur si celui-ci existe ou null
            } else if (message[0].equals("connexion")) {
                Log.d("serveur", "************Connexion :" + message[1]);
                //vérifier le contenu
                try {
                    JSONObject jSonInfo = new JSONObject(message[1]);
                    Global.id = jSonInfo.getString("id");
                    System.out.println(Global.id);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if (message[0].equals("enregistrer")) {
                Log.d("serveur", "***********enregistrer :" +message[1]);
            }
        }
    }

    /**
     * Envoi vers le serveur distant (vers la page PHP)
     * @param operation
     * @param lesDonneesJSON
     */
    public void envoi(String operation, JSONArray lesDonneesJSON) {
        // création de l'objet d'accès à distance
        AccesHTTP accesDonnees = new AccesHTTP();
        accesDonnees.delegate = this;
        // ajout des données en paramêtre
        accesDonnees.addParam("android", operation);
        accesDonnees.addParam("lesDonnees", lesDonneesJSON.toString());
        // envoi
        accesDonnees.execute(SERVERADDR);
    }


}
