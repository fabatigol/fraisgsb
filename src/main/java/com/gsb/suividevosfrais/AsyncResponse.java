package com.gsb.suividevosfrais;

/**
 * Created by Fabien on 15/12/2016.
 */


public interface AsyncResponse {
    void processFinish(String output);
}
